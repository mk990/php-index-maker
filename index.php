<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body {
            margin: 0;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            text-align: left;
            background-color: #111;
            color: #ccc;
        }

        table {
            border-collapse: collapse;
        }

        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
        }

        .table th,
        .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
        }

        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
        }

        .table tbody + tbody {
            border-top: 2px solid #dee2e6;
        }

        .table-dark,
        .table-dark > th,
        .table-dark > td {
            background-color: #c6c8ca;
        }

        .table-dark th,
        .table-dark td,
        .table-dark thead th,
        .table-dark tbody + tbody {
            border-color: #95999c;
        }

        .table-hover .table-dark:hover {
            background-color: #b9bbbe;
        }

        .table-hover .table-dark:hover > td,
        .table-hover .table-dark:hover > th {
            background-color: #b9bbbe;
        }

        .table-dark {
            color: #fff;
            background-color: #212529;
        }

        .table-dark th,
        .table-dark td,
        .table-dark thead th {
            border-color: #32383e;
        }

        .table-dark {
            border: 0;
        }

        .table-dark.table-striped tbody tr:nth-of-type(odd) {
            background-color: rgba(255, 255, 255, 0.05);
        }

        .table-dark.table-hover tbody tr:hover {
            color: #fff;
            background-color: rgba(255, 255, 255, 0.075);
        }

        .text-center {
            text-align: center !important;
        }

        .container {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        @media (min-width: 576px) {
            .container {
                max-width: 540px;
            }
        }

        @media (min-width: 768px) {
            .container {
                max-width: 720px;
            }
        }

        @media (min-width: 992px) {
            .container {
                max-width: 960px;
            }
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 1140px;
            }
        }

        body {
            font-family: "Roboto Slab", serif;
            margin: 0;
        }

        a {
            color: #ccc;
            text-decoration: none;
        }

        a span {
            font-weight: 100;
            transition: all 0.5s;
        }

        a span:nth-child(2) {
            transition-delay: 0.1s;
        }

        a span:nth-child(3) {
            transition-delay: 0.2s;
        }

        a span:nth-child(4) {
            transition-delay: 0.3s;
        }

        a span:nth-child(5) {
            transition-delay: 0.4s;
        }

        a span:nth-child(6) {
            transition-delay: 0.5s;
        }

        a span:nth-child(7) {
            transition-delay: 0.6s;
        }

        a span:nth-child(8) {
            transition-delay: 0.7s;
        }

        a span:nth-child(9) {
            transition-delay: 0.8s;
        }

        a span:nth-child(10) {
            transition-delay: 0.9s;
        }

        a:hover span {
            font-weight: 700;
        }

        a:hover span:nth-child(10) {
            transition-delay: 0.9s;
        }

        a:hover span:nth-child(9) {
            transition-delay: 0.8s;
        }

        a:hover span:nth-child(8) {
            transition-delay: 0.7s;
        }

        a:hover span:nth-child(7) {
            transition-delay: 0.6s;
        }

        a:hover span:nth-child(6) {
            transition-delay: 0.5s;
        }

        a:hover span:nth-child(5) {
            transition-delay: 0.4s;
        }

        a:hover span:nth-child(4) {
            transition-delay: 0.3s;
        }

        a:hover span:nth-child(3) {
            transition-delay: 0.2s;
        }

        a:hover span:nth-child(2) {
            transition-delay: 0.1s;
        }

        a:hover span:nth-child(1) {
            transition-delay: 0s;
        }
    </style>
</head>
<body>
<h1 class="text-center py-3">Files for downloads</h1>

<div class="container">
    <table class="table table-dark">
        <tr>
            <th class="col-1">#</th>
            <th class="col-6">File</th>
            <th class="col-2">Size</th>
            <th class="col-3">Date</th>
        </tr>
        <?php

        /**
         * @param $bytes
         * @return float|int|string
         */
        function FileSizeConvert($bytes)
        {
            $bytes = floatval($bytes);
            $arBytes = array(
                0 => array(
                    "UNIT"  => "TB",
                    "VALUE" => pow(1024, 4)
                ),
                1 => array(
                    "UNIT"  => "GB",
                    "VALUE" => pow(1024, 3)
                ),
                2 => array(
                    "UNIT"  => "MB",
                    "VALUE" => pow(1024, 2)
                ),
                3 => array(
                    "UNIT"  => "KB",
                    "VALUE" => 1024
                ),
                4 => array(
                    "UNIT"  => "B",
                    "VALUE" => 1
                ),
            );
            $result = 0;
            foreach ($arBytes as $arItem) {
                if ($bytes >= $arItem["VALUE"]) {
                    $result = $bytes / $arItem["VALUE"];
                    $result = str_replace(".", ",", strval(round($result, 2))) . " " . $arItem["UNIT"];
                    break;
                }
            }
            return $result;
        }

        $link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $allFiles = scandir(__DIR__); // Or any other directory
        $files = array_diff($allFiles, ['.', '..', '.gitignore', '.git', 'index.php']);
        $num = 1;
        foreach ($files as $x) {
            $filePath = __DIR__ . '/' . $x;
            $fileSize = FileSizeConvert(fileSize($filePath));
            $fileCreatedAt = date("Y-m-d H:i:s", filemtime($filePath));
            $spanX = "";
            for ($i = 0; $i < strlen($x); $i++) {
                $spanX .= "<span>$x[$i]</span>";
            }
            echo "
                  <tr>
                        <td>$num</td>
                        <td>
                             <a href='$link$x'>$spanX</a>
                        </td>
                        <td>$fileSize</td>
                        <td>$fileCreatedAt</td>
                  </tr>
                  ";
            $num++;
        }
        ?>
    </table>
</div>
</body>

</html>
